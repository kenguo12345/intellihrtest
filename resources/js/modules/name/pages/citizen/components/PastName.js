// import libs
import React from "react";
import PropTypes from "prop-types";

// import components

const displayName = "NameComponent";
const propTypes = {
  index: PropTypes.number.isRequired,
  name: PropTypes.object.isRequired,
};

function render({ name }) {
  return (
    <div className="col-12 col-sm-9 mb-5 mx-auto">
      <li className="card-text">{name.value}</li>
    </div>
  );
}

render.displayName = displayName;
render.propTypes = propTypes;

export default render;
