import moment from "moment";
import Model from "../../utils/Model";
import User from "../user/User";

class Name extends Model {
  constructor(props) {
    super(props);

    this.initialize(props);
  }

  initialize(props) {
    super.initialize(props);

    this.value = props.value || "";

    // relate user model
    this.user = props.user ? new User(props.user) : null;
  }
}

export default Name;
