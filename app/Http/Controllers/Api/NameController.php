<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\NameRequest;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Name;
use App\User;

class NameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return LengthAwarePaginator|mixed
     */
    public function index(Request $request)
    {
        if ($request->user()->is_admin) {
            return Name::loadAll();
        }
        return Name::loadAllMine($request->user()->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NameRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NameRequest $request, $id)
    {
        $name = Name::findOrFail($id);

        $data = $request->validated();
        $data['slug'] = Str::slug($data['title']);
        $name->update($data);

        return response()->json($name, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * get all expiring names
     *
     * @return mixed
     */
    public function expiringNames()
    {
        return Name::loadAllExpiring();
    }

    /**
     * get all inactive names
     *
     * @return mixed
     */
    public function inactiveNames()
    {
        return Name::loadAllUsedNames();
    }

    /**
     * get user current name
     *
     * @param Request $request
     * @return mixed
     * 
     *
     */
    public function currentName(Request $request)
    {
        return Name::where('user_id', '=', $request->user()->id)->first();
    }



    /**
     * get all past names
     *
     * @param Request $request
     * @return mixed
     * 
     *
     */
    public function pastNames(Request $request)
    {

        return Name::loadAllPastNames($request->user()->id);
    }





    /**
     * get all past names with id
     *
     * @param Request $request
     * @return mixed
     * 
     *
     */
    public function sudoPastNames(Request $request, int $user_id)
    {
        if (!$request->user()->is_admin) {
            return response()->json('access deny', 403);
        }
        return Name::loadAllPastNames($user_id);
    }

    /**
     * change name for logined user
     *
     * @param Request $request
     * @return mixed
     * 
     *
     */
    public function changeName(NameRequest $request)
    {
        $user_id = $request->user()->id;

        $user = User::findOrFail($user_id);
        $name = $request->name;
        $used_name_exist = $this->usedNameExist($user_id,$name);
        $current_name_exist = $this->currentNameExist($user_id);
        
        if($used_name_exist)
        {
            return response()->json([
                'error' => 'name has been used in the past'
            ], 400);
        }
        if($current_name_exist)
        {
            return response()->json([
                'error' => 'name is being used by other people'
            ], 400);
        }

        $this->updateAndStore($user_id,$name);
        
        return response()->json('created', 200);
    }
    
    /**
     * change to new name with id and admin right
     *
     * @param Request $request
     * @return mixed
     * 
     *
     */
    public function sudoChangeName(NameRequest $request,int $user_id)
    {
        //if not admin, check if user id same as request user_ids

        if($user_id !== $request->user()->id)
        {
            if (!$request->user()->is_admin) {
                return response()->json('access deny', 403);
            } 
        }

        
        $user = User::findOrFail($user_id);
        $name = $request->name;
        $used_name_exist = $this->usedNameExist($user_id,$name);
        $current_name_exist = $this->currentNameExist($user_id);
        
        if($used_name_exist)
        {
            return response()->json([
                'error' => 'name has been used in the past'
            ], 400);
        }
        if($current_name_exist)
        {
            return response()->json([
                'error' => 'name is being used by other people'
            ], 400);
        }

        $this->updateAndStore($user_id,$name);
        
        return response()->json('created', 200);

    }

    private function usedNameExist(int $user_id, string $name)
    {
        $used_name_match_count = Name::where('used_user_id',$user_id)
            ->where('value',$name)
            ->count();
        if($used_name_match_count>0)
            return true;       
        else
            return false;

    }
    private function currentNameExist(string $name)
    {
        $current_name_match_count = Name::whereNotNull('user_id')
            ->where('value',$name)
            ->count();   
        
        if($current_name_match_count>0)
            return true;       
        else
            return false;
                    
    }
    private function updateAndStore(int $user_id, string $name)
    {
        $old_name = Name::where('user_id',$user_id);
        $new_name = new Name;

        $old_name->update(['user_id' => null]);

        $new_name->value = $name;
        $new_name->used_user_id= $user_id;
        $new_name->user_id= $user_id;
        $new_name->save();


    }
    private function isAdmin(Request $request)
    {
        if (!$request->user()->is_admin) {
            return false;
        }

        return true;
    }
}
