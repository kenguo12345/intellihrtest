<?php

use Illuminate\Support\Facades\Route;

Route::get('/expiring', 'NameController@expiringNames')->name('names.expiring');

Route::group(['middleware' => 'auth:api'], function() {
    #common
    Route::get('/pastnames', 'NameController@pastNames')->name('names.past');
    
    Route::get('/currentname', 'NameController@currentName')->name('names.current');
    Route::post('/changename', 'NameController@changeName')->name('names.change');


    #offical

    Route::get('/pastnames/{id}', 'NameController@sudoPastNames')->name('names.sudoPastname');
    Route::post('/changename/{id}', 'NameController@sudoChangeName')->name('names.sudoChange');
    



  //  Route::post('/', 'NameController@store')->name('names.store');
    Route::get('/', 'NameController@index')->name('names.index');
  //  Route::get('/{id}', 'NameController@show')->name('names.show');
  //  Route::match(['put', 'patch'], '/{id}', 'NameController@update')->name('names.update');
   // Route::delete('/{id}', 'NameController@delete')->name('names.delete');
});