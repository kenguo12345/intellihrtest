<?php

namespace Tests\Feature;

use App\User;
use App\Name;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;

class NameTest extends TestCase
{
    use DatabaseTransactions;

    public $user;
    public $citizen;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = $this->createAdminUser();
        $this->citizen = $this->createCitizen();
    }


    private function createActiveName()
    {
        return Name::create([
            'value' => 'Timmy',
            'created_at' => \Carbon\Carbon::now()->subDays(360),
            'updated_at' => \Carbon\Carbon::now()->subDays(360),
            'user_id' => 4,
            'used_user_id' => 2,
            'pending' => false
        ]);
    }

    private function createAdminUser()
    {
        return User::create([
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'is_admin' => true,
            'remember_token' => Str::random(10),
        ]);
    }
    private function createCitizen()
    {
        return User::create([
            'email' => 'citizen@gmail.com',
            'password' => bcrypt('secret'),
            'is_admin' => false,
            'remember_token' => Str::random(10),
        ]);
    }

    public function that_load_expiring_names()
    {
        $response = $this->get('/expiring');

        $response
            ->assertStatus(200);

    }








}
